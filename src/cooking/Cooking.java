package cooking;

import exceptions.CookingException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Chef;
import model.Dish;
import persistence.CookingDAO;

/**
 * JDBC
 *
 * @author mfontana
 */
public class Cooking {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            CookingDAO cookingDAO = new CookingDAO();
            System.out.println("Estableciendo conexión con la bbdd...");
            cookingDAO.connect();
            System.out.println("Conexión establecida");
            System.out.println("Test de insert cocinero");
            Chef c = new Chef("Limon Pepe", "123456789", "Hombre", 50, 33, "postres");
            try {
                cookingDAO.insertChef(c);
                System.out.println("Chef insertado en la bbdd");
            } catch (CookingException ex) {
                System.out.println(ex.getMessage());
            }
            Dish d = new Dish("Arroz tres delicias", "Plato principal", 8.5, c);
            try {
                cookingDAO.insertDish(d);
                System.out.println("Plato registrado en la bbdd");
            } catch (CookingException ex) {
                System.out.println(ex.getMessage());
            }
            System.out.println("Traemos al cocinero con el nombre Alba Cete");
            try {
                Chef c2 = cookingDAO.getChefByName("Alba Cete");
                System.out.println(c2);
            } catch (CookingException ex) {
                System.out.println(ex.getMessage());
            }
            cookingDAO.disconnect();
            System.out.println("Cerrada conexión");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

}
