package persistence;

import exceptions.CookingException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import model.Chef;
import model.Dish;

/**
 * DAO
 *
 * @author mfontana
 */
public class CookingDAO {

    Connection connection;

    public Chef getChefByName(String name) throws SQLException, CookingException {
        if (!existChef(new Chef(name))) {
             throw new CookingException("No existe cocinero con ese nombre");
        }
        Statement st = connection.createStatement();
        String select = "select * from cocinero where nombre = '" + name + "'";
        ResultSet rs = st.executeQuery(select);
        Chef c = new Chef();
        if (rs.next()) {
            c.setName(rs.getString("nombre"));
            c.setAge(rs.getInt("edad"));
            c.setExperience(rs.getInt("experiencia"));
            c.setGenre(rs.getString("sexo"));
            c.setPhone(rs.getString("telefono"));
            c.setSpecialty(rs.getString("especialidad"));
        }
        rs.close();
        st.close();
        return c;
    }

    public void insertDish(Dish d) throws SQLException, CookingException {
        if (existDish(d)) {
            throw new CookingException("Ya existe el plato");
        }
        if (!existChef(d.getChef())) {
            throw new CookingException("No existe el cocinero que indicas en el plato");
        }
        PreparedStatement ps = connection.prepareStatement("insert into plato values (?, ?, ?, ?)");
        ps.setString(1, d.getName());
        ps.setString(2, d.getType());
        ps.setDouble(3, d.getPrice());
        ps.setString(4, d.getChef().getName());
        ps.executeUpdate();
        ps.close();
    }

    public void insertChef(Chef c) throws SQLException, CookingException {
        if (existChef(c)) {
            throw new CookingException("Ya existe el cocinero");
        }
        PreparedStatement ps = connection.prepareStatement("insert into cocinero values (?, ?, ?, ?, ?, ?)");
        ps.setString(1, c.getName());
        ps.setString(2, c.getPhone());
        ps.setString(3, c.getGenre());
        ps.setInt(4, c.getAge());
        ps.setInt(5, c.getExperience());
        ps.setString(6, c.getSpecialty());
        ps.executeUpdate();
        ps.close();
    }

    private boolean existDish(Dish d) throws SQLException {
        Statement st = connection.createStatement();
        String query = "Select * from plato where nombre = '" + d.getName() + "'";
        ResultSet rs = st.executeQuery(query);
        boolean existe = rs.next();
        rs.close();
        st.close();
        return existe;
    }

    private boolean existChef(Chef c) throws SQLException {
        Statement st = connection.createStatement();
        String query = "select * from cocinero where nombre ='" + c.getName() + "'";
        ResultSet rs = st.executeQuery(query);
        boolean existe = rs.next();
        rs.close();
        st.close();
        return existe;
    }

    public void connect() throws SQLException {
        String url = "jdbc:mysql://localhost:3306/restaurant";
        String user = "root";
        String pass = "root";
        connection = DriverManager.getConnection(url, user, pass);
    }

    public void disconnect() throws SQLException {
        if (connection != null) {
            connection.close();
        }
    }

}
