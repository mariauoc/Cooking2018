package model;

/**
 *
 * @author mfontana
 */
public class Dish {

    private String name;
    private String type;
    private double price;
    private Chef chef;

    public Dish() {
    }

    public Dish(String name, String type, double price, Chef chef) {
        this.name = name;
        this.type = type;
        this.price = price;
        this.chef = chef;
    }

    /**
     * Get the value of chef
     *
     * @return the value of chef
     */
    public Chef getChef() {
        return chef;
    }

    /**
     * Set the value of chef
     *
     * @param chef new value of chef
     */
    public void setChef(Chef chef) {
        this.chef = chef;
    }

    /**
     * Get the value of price
     *
     * @return the value of price
     */
    public double getPrice() {
        return price;
    }

    /**
     * Set the value of price
     *
     * @param price new value of price
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * Get the value of type
     *
     * @return the value of type
     */
    public String getType() {
        return type;
    }

    /**
     * Set the value of type
     *
     * @param type new value of type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Dish{" + "name=" + name + ", type=" + type + ", price=" + price + ", chef=" + chef + '}';
    }

}
