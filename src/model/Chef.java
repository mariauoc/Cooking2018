package model;

/**
 *
 * @author mfontana
 */
public class Chef {

    private String name;
    private String phone;
    private String genre;
    private int age;
    private int experience;
    private String specialty;

    public Chef() {
    }

    public Chef(String name) {
        this.name = name;
    }

    public Chef(String name, String phone, String genre, int age, int experience, String specialty) {
        this.name = name;
        this.phone = phone;
        this.genre = genre;
        this.age = age;
        this.experience = experience;
        this.specialty = specialty;
    }
    
    
    
    /**
     * Get the value of specialty
     *
     * @return the value of specialty
     */
    public String getSpecialty() {
        return specialty;
    }

    /**
     * Set the value of specialty
     *
     * @param specialty new value of specialty
     */
    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    /**
     * Get the value of experience
     *
     * @return the value of experience
     */
    public int getExperience() {
        return experience;
    }

    /**
     * Set the value of experience
     *
     * @param experience new value of experience
     */
    public void setExperience(int experience) {
        this.experience = experience;
    }

    /**
     * Get the value of age
     *
     * @return the value of age
     */
    public int getAge() {
        return age;
    }

    /**
     * Set the value of age
     *
     * @param age new value of age
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Get the value of genre
     *
     * @return the value of genre
     */
    public String getGenre() {
        return genre;
    }

    /**
     * Set the value of genre
     *
     * @param genre new value of genre
     */
    public void setGenre(String genre) {
        this.genre = genre;
    }

    /**
     * Get the value of phone
     *
     * @return the value of phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Set the value of phone
     *
     * @param phone new value of phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Get the value of name
     *
     * @return the value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Set the value of name
     *
     * @param name new value of name
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Chef{" + "name=" + name + ", phone=" + phone + ", genre=" + genre + ", age=" + age + ", experience=" + experience + ", specialty=" + specialty + '}';
    }

    
    
}
