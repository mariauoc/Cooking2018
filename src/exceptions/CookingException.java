/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptions;

/**
 *
 * @author mfontana
 */
public class CookingException extends Exception {

    public CookingException(String message) {
        super(message);
    }

}
